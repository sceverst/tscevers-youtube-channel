@set FF="C:\avrdude\avrdude.exe"
@REM set FF="C:\avrdude\avrdude.exe"
@REM set FF="C:\avrdude\avrdude.exe"
@REM set FLAGS=-b -t -J
@if %1/==/ goto Usage
%FF% %FLAGS% %*
@if %ERRORLEVEL% equ 0 goto exit 
@goto wait
:Usage
@echo Usage:%0 -p ATmega328p -c arduino -P com3 -b 57600 -U flash:w:hexfile.hex:i 
@echo default:	%0 %FLAGS% ... 
:wait
@pause
:exit 
@set FF=
@set FLAGS=
