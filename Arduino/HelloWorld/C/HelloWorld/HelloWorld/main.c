#include <avr/io.h>
#include <stdbool.h>
#include <stdio.h>
#include <util/delay.h>
#include "uart.h"

#define LED (1 << DDB5)

void setup( void );

int main(void)
{
	setup();
	
	// Create File Pointers for in and out
	FILE uart_output = FDEV_SETUP_STREAM(uart_putchar, NULL, _FDEV_SETUP_WRITE);
	FILE uart_input = FDEV_SETUP_STREAM(NULL, uart_getchar, _FDEV_SETUP_READ);
	
	// Hijack STDIN/STDOUT and send then to the UART Driver
	stdout = &uart_output;
	stdin  = &uart_input;
	
	while( true )
	{
		printf("Hello C World!!!\n");
		PORTB |= LED;
		_delay_ms(500);
		PORTB &= ~LED;
		_delay_ms(500);
	}
}

void setup( void )
{
	// Turn LED into an output
	DDRB |= LED;
	
	// Initialize the UART
	uart_init();
}

