module toggle (
	input clk,
	input pulse,
	output reg led
);

always @( posedge clk )
	begin
		if( pulse )
			led <= ~led;
		else
			led <= led;
	end
endmodule

