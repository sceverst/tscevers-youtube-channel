module levelToPulse (
	input clk,
	input level,
	input reset,
	output reg pulse
);

parameter [1:0]	NoSignal = 2'b00,
                  SignalDetected = 2'b01,
						Hold = 2'b11;

reg [1:0] state;

always @( posedge clk )
	begin
		if ( reset )
			state <= NoSignal;
		else
			case( state )
				NoSignal :
					if( level )
						state <= SignalDetected;
					else
						state <= state;
				SignalDetected :
					if( level )
						state <= Hold;
					else
						state <= NoSignal;
				Hold :
					if( level )
						state <= state;
					else
						state <= NoSignal;
				default : state <= NoSignal;
			endcase
	end
always @( state )
	begin
		case( state )
			NoSignal : pulse <= 0;
			SignalDetected : pulse <= 1;
			Hold : pulse <= 0;
			default : pulse <= 0;
		endcase
	end
endmodule