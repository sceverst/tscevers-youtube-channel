module SignalTapDemo (
	input button,
	input clk,
	input reset,
	output led
);

wire pulse;

levelToPulse		U1	(clk, ~button, ~reset, pulse);
toggle				U2	(clk, pulse, led);

endmodule
