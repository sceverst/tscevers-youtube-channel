#include <stdio.h>
#include <system.h>
#include <io.h>

int main( void )
{
	printf("Welcome to the custom peripheral tutorial in Nios II!");

	IOWR(LEDS_BASE, 0, 0x3ff);

	printf("\nCustom Peripheral: 0x%x", IORD(LEDS_BASE, 0));

	while ( 1 )
	{ }

	return 0;
}
