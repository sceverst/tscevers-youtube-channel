# Projects related to the DE10-Standard developement board from Terasic
[Visit DE10-Standard Page](https://www.terasic.com.tw/cgi-bin/page/archive.pl?Language=English&CategoryNo=165&No=1081#contents)

## Index of Projects

### Nios II softcore processor system examples
* HelloNios     			(Introduction to the Nios II Softcore Processor)
* HelloNiosPIO  			(Adding a general Parallel I/O core to the Hello World Example)
* HelloNiosIntr 			(Adding Interrupts to the Nios II Processor system)
* HelloFreeRTOS 			(Running FreeRTOS on the Nios II processor)
* NiosCustomIP1 			(Adding Custom IP core to Nios II processor)
* NiosCustomIP2				(Adding Custom IP core to Nios II processor)
* NiosCustomInstruction		(Creating a custom instruction for the Nios II system)

### hard Processor System (HPS) examples
* HelloWorld_HPS			(Using the GHRD to write a program for HPS)
* HPS_HelloPIO				(Creating your own custom HPS system)
