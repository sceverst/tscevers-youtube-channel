#include <stdio.h>
#include <system.h>
#include <io.h>

int main ( void )
{
	printf("Welcome to Custom IP Demo 2!\n");

	IOWR(MY_REG_BASE, 0, 0xAAAAAAAA);
	IOWR(MY_REG_BASE, 1, 0x55555555);
	IOWR(MY_REG_BASE, 2, 0xBEEFBEEF);

	printf("Custom Reg 1: 0x%x\n", IORD(MY_REG_BASE, 0));
	printf("Custom Reg 2: 0x%x\n", IORD(MY_REG_BASE, 1));
	printf("Custom Reg 3: 0x%x\n", IORD(MY_REG_BASE, 2));

	IOWR(MY_REG_BASE, 1, 0xFFFFFFFF);

	printf("Custom Reg 1: 0x%x\n", IORD(MY_REG_BASE, 0));
	printf("Custom Reg 2: 0x%x\n", IORD(MY_REG_BASE, 1));
	printf("Custom Reg 3: 0x%x\n", IORD(MY_REG_BASE, 2));

	while ( 1 )
	{ }

	return 0;
}
