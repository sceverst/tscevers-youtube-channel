#include <stdio.h>
#include "system.h"
#include "altera_avalon_pio_regs.h"

int main()
{ 
  printf("Hello from Nios II!\n");

  //  Set a value in the PIO core
  //  Turn on LEDs
  IOWR_ALTERA_AVALON_PIO_DATA(LEDS_BASE, 0x3ff);

  //Read and print the value of the PIO core
  printf("LEds value: 0x%x", IORD_ALTERA_AVALON_PIO_DATA(LEDS_BASE));

  /* Event loop never exits. */
  while (1)
  { }

  return 0;
}
