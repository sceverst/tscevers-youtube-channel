
module HelloNiosPIO (
	clk_clk,
	leds_export);	

	input		clk_clk;
	output	[9:0]	leds_export;
endmodule
