#include <asf.h>
#include <stdbool.h>

#include "myTasks.h"

void myButtonTask( void * pvParameters )
{
	while( true )
	{
		// Is the button pressed
		if( ioport_get_pin_level(BUTTON_0_PIN) == BUTTON_0_ACTIVE )
		{
			ioport_set_pin_level(LED_0_PIN, LED_0_ACTIVE);
		}
		else
		{
			ioport_set_pin_level(LED_0_PIN, LED_0_INACTIVE);			
		}
	}
}