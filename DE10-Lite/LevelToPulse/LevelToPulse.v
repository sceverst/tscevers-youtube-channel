module LevelToPulse (
	input clk,
	input reset,
	input level,
	output reg pulse
);

	// Name the states with a Parameter
	parameter [1:0] idle = 2'b00,
						 levelDetected = 2'b01,
						 levelHold = 2'b11;
	
	// Create memory for the state machine
	reg [1:0] state;
	
	// Always Block to Handle State Machine
	always @( posedge clk )
	begin
		if ( reset )
			state <= idle;
		else
			case ( state )
				idle :
					if ( level )
						state <= levelDetected;
					else
						state <= state;
				levelDetected :
					if ( level )
						state <= levelHold;
					else
						state <= idle;
				levelHold :
					if ( level )
						state <= state;
					else
						state <= idle;
				default : state <= idle;
			endcase
	end
	
	// Always Block to Handle Output
	always @( state )
	begin
		case ( state )
			idle : pulse <= 1'b0;
			levelDetected : pulse <= 1'b1;
			levelHold : pulse <= 1'b0;
			default : pulse <= 1'b0;
		endcase
	end
endmodule
